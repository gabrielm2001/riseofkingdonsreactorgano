import "./App.css";
import Form from "./Components/Form";
import Header from "./Components/Header";

function App() {
  return (
    <div className="App">
      <Header></Header>
      <Form></Form>
    </div>
  );
}

export default App;
