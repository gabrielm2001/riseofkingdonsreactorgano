import "./Form.css";
import InputsFields from "../InputsFields";
import SelectField from "../SelectField";
import BtnSend from "../BtnSend";
import { useState } from "react";

const Form = (props) => {
  const inputsField = [
    {
      nome: "Governador",
    },
    {
      nome: "Nível vip",
    },
    {
      nome: "Poder",
    },
  ];

  const comandantes = [
    {
      raridade: "Lendário",
      comandante: "Alexandre O grande",
      comandanteImg: "../../../images/Alexander_the_Great",
      logo: "../../../images/castelo",
      especialidade1: "Infantaria",
      especialidade2: "Versatividade",
      especialidade3: "Ataque",
      especialidade1Img: "../../../images/infantaria",
      especialidade2Img: "../../../images/versatividade",
      especialidade3Img: "../../../images/ataque",
    },

    {
      raridade: "Lendário",
      comandante: "Attila",
      comandanteImg: "../../../images/attila",
      logo: "../../../images/castelo",
      especialidade1: "Infantaria",
      especialidade2: "Versatividade",
      especialidade3: "Ataque",
      especialidade1Img: "../../../images/cavalaria",
      especialidade2Img: "../../../images/conquistador",
      especialidade3Img: "../../../images/ataque",
    },

    {
      raridade: "Lendário",
      comandante: "Cao Cao",
      comandanteImg: "../../../images/Cao_Cao",
      logo: "../../../images/dragao",
      especialidade1: "Cavalaria",
      especialidade2: "Versatividade",
      especialidade3: "Ataque",
      especialidade1Img: "../../../images/infantaria",
      especialidade2Img: "../../../images/versatividade",
      especialidade3Img: "../../../images/ataque",
    },
    // ----------------------------------------------------------------------------
    {
      raridade: "Épico",
      comandante: "Pelágio",
      comandanteImg: "../../../images/pelagio",
      logo: "../../../images/touro",
      especialidade1: "cavalaria",
      especialidade2: "guarnicao",
      especialidade3: "skill",
      especialidade1Img: "../../../images/cavalaria",
      especialidade2Img: "../../../images/guarnicao",
      especialidade3Img: "../../../images/skill",
    },

    {
      raridade: "Épico",
      comandante: "Boudica",
      comandanteImg: "../../../images/boudica",
      logo: "../../../images/coroa",
      especialidade1: "integracao",
      especialidade2: "paz",
      especialidade3: "skill",
      especialidade1Img: "../../../images/integracao",
      especialidade2Img: "../../../images/paz",
      especialidade3Img: "../../../images/skill",
    },

    {
      raridade: "Épico",
      comandante: "Keira",
      comandanteImg: "../../../images/keira",
      logo: "../../../images/castelo",
      especialidade1: "arquearia",
      especialidade2: "paz",
      especialidade3: "skill",
      especialidade1Img: "../../../images/arquearia",
      especialidade2Img: "../../../images/paz",
      especialidade3Img: "../../../images/skill",
    },
    //----------------------------------------------------------------------------------------
    {
      raridade: "Elite",
      comandante: "Lancelote",
      comandanteImg: "../../../images/lancelote",
      logo: "../../../images/coroa",
      especialidade1: "cavalaria",
      especialidade2: "versatividade",
      especialidade3: "mobilidade",
      especialidade1Img: "../../../images/cavalaria",
      especialidade2Img: "../../../images/versatividade",
      especialidade3Img: "../../../images/mobilidade",
    },
    {
      raridade: "Elite",
      comandante: "Galio Magno",
      comandanteImg: "../../../images/GalioMagno",
      logo: "../../../images/roma",
      especialidade1: "lideranca",
      especialidade2: "coleta",
      especialidade3: "skill",
      especialidade1Img: "../../../images/lideranca",
      especialidade2Img: "../../../images/coleta",
      especialidade3Img: "../../../images/skill",
    },
    {
      raridade: "Elite",
      comandante: "outo",
      comandanteImg: "../../../images/keira",
      logo: "../../../images/castelo",
      especialidade1: "arquearia",
      especialidade2: "paz",
      especialidade3: "skill",
      especialidade1Img: "../../../images/arquearia",
      especialidade2Img: "../../../images/paz",
      especialidade3Img: "../../../images/skill",
    },
    // --------------------------------------------------------------------------------------------
  ];

  const comandanteNome = comandantes.map((comandante, index) => {
    return {
      comandante: comandante.comandante,
      index: index,
    };
  });

  const [governador, setGovernador] = useState("");
  const [vip, setVip] = useState("");
  const [poder, setpoder] = useState("");
  const [lider, setLider] = useState("");

  const changedInput = (target) => {
    switch (target.placeholder) {
      case "Governador":
        setGovernador(target.value);
        console.log(target.value);
        break;
      case "Nível vip":
        setVip(target.value);
        break;
      case "Poder":
        setpoder(target.value);
        break;
      default:
        break;
    }
  };

  const changedSelect = (lider) => {
    console.log(lider);
    setLider(lider);
  };

  const saveChanges = (event) => {
    event.preventDefault();

    let comandanteEscolhido = comandantes.filter(
      (comandante) => comandante.comandante === lider
    );
    let comandanteEscolhido2 = comandanteEscolhido[0];

    console.log(comandanteEscolhido);

    let comandanteCard = [
      {
        raridade: comandanteEscolhido2.raridade,
        lider,
        imagem: comandanteEscolhido2.comandanteImg,
        especialidade1: comandanteEscolhido2.especialidade1,
        especialidade2: comandanteEscolhido2.especialidade2,
        especialidade3: comandanteEscolhido2.especialidade3,
        especialidade1Img: comandanteEscolhido2.especialidade1Img,
        especialidade2Img: comandanteEscolhido2.especialidade2Img,
        especialidade3Img: comandanteEscolhido2.especialidade3Img,
        logo: comandanteEscolhido2.logo,
        poder,
        vip,
        governador,
      },
    ];

    console.log(comandanteCard);
  };

  return (
    <section className="formSection">
      <h3>Governador escolha seu comandante</h3>
      <form onSubmit={saveChanges}>
        {inputsField.map((inputField) => (
          <InputsFields
            nome={inputField.nome}
            key={inputField.nome}
            changedInput={(target) => {
              changedInput(target);
            }}
            required={true}
          ></InputsFields>
        ))}
        <SelectField
          nomes={comandanteNome}
          comandanteSelecionado={changedSelect}
        ></SelectField>
        <BtnSend></BtnSend>
      </form>
    </section>
  );
};

export default Form;
