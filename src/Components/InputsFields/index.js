import "./InputsFields.css";

const InputsFields = ({ nome, changedInput, required }) => {
  const onChangeInput = (event) => {
    changedInput(event.target);
  };

  return (
    <section key={nome} className="inputSection">
      <label>{nome}</label>
      <input
        type="text"
        placeholder={nome}
        onChange={onChangeInput}
        required={required}
      />
    </section>
  );
};

export default InputsFields;
