import "./SelectField.css";

const SelectField = ({ nomes, comandanteSelecionado }) => {
  return (
    <select
      key={nomes.comandante}
      className="selectSection"
      onChange={(event) => comandanteSelecionado(event.target.value)}
    >
      {nomes.map((nome) => (
        <option key={nome.comandante}>{nome.comandante}</option>
      ))}
    </select>
  );
};

export default SelectField;
